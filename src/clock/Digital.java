package clock;

import model.Observer;
import time.TimeServer;

import java.beans.PropertyChangeEvent;

public class Digital extends Observer {

    public Digital(TimeServer server){
        server.Attach(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        long time = (long) event.getNewValue();
        System.out.printf("%02d:%02d:%02d\r", time / 3600 % 24, time / 60 % 60, time % 60);
    }
}
