package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public abstract class Observable {
    protected PropertyChangeSupport listeners;
    public Observable(){
        listeners = new PropertyChangeSupport(this);
    }
    public void Attach(PropertyChangeListener listener){
        listeners.addPropertyChangeListener(listener);
    }
    public void Detach(PropertyChangeListener listener){
        listeners.removePropertyChangeListener(listener);
    }

}
