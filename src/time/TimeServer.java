package time;

import model.Observable;

import java.util.Date;

public class TimeServer extends Observable implements Runnable {
    private long timeState;

    public TimeServer(){
        super();
        timeState = new Date().getTime()/1000;
    }

    private void increment() {
        long oldState = timeState;
        timeState++;
        listeners.firePropertyChange("time", oldState, timeState);
    }

    @Override
    public void run() {
        while (true){
            try{
                Thread.sleep(1000);
                increment();
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
