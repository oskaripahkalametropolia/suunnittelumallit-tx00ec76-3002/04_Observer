import clock.Digital;
import time.TimeServer;

public class Main {
    public static void main(String[] args) {
        TimeServer server = new TimeServer();
        Thread serverThread = new Thread(server);
        serverThread.start();

        Digital digitalClock = new Digital(server);
    }
}
